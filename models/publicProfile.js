const mongoose = require("mongoose");

const userProfileSchema = new mongoose.Schema({
  name: { type: String, required: true },
  occupation: { type: String, required: true },
  color: { type: String, required: true },
  profilePic: { type: String },
  userID: { type: String },
  profileCode: { type: String },
  socialLinks: {
    twitter: { type: String },
    linkedin: { type: String },
    instagram: { type: String },
    facebook: { type: String },
    github: { type: String },
    youtube: { type: String },
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

const UserProfile = mongoose.model("UserProfile", userProfileSchema);
module.exports = UserProfile;
