const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  ip: String,
  browser: String,
  language: String,
  country: String,
  region: String,
  city: String,
  visit: Number,
  date: {
    type: String,
    default: Date.now,
  },
});

module.exports = mongoose.model("users", userSchema);
