const express = require("express");
const connectDb = require("./config/db");
const cors = require("cors");
var geoip = require("geoip-lite");
require("dotenv").config();
const Users = require("./models/users");
const UserProfile = require("./models/publicProfile");
const axios = require("axios");

const app = express();
app.set("trust proxy", true);

//regular middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//cors
app.use(cors());

//port
const PORT = process.env.PORT || 5000;

//connect to database
connectDb();

//api

app.post("/", async (req, res) => {
  try {
    const response = await axios.get("https://ipapi.co/json");
    const data = response?.data;

    const checkIfExist = await Users.findOne({
      ip: data?.ip,
    });

    if (checkIfExist) {
      checkIfExist.visit = checkIfExist.visit + 1;
      checkIfExist.save();
      return res.status(200).json("added to database");
    }
    //ip lookup
    //let geo = geoip.lookup(ip);

    //track user info
    const user_info = await Users.create({
      ip: data?.ip,
      browser: req.headers["user-agent"],
      language: req.headers["accept-language"],
      country: data ? data?.country_name : "Unknown",
      region: data ? data?.region : "Unknown",
      city: data ? data?.city : "Unknown",
      visit: 1,
    });

    res.status(200).json("added to database");
  } catch (err) {
    console.error(err);
    res.status(500).json("Server error");
  }
});

//save the user public profile
app.post("/saveProfile", async (req, res) => {
  const { name, occupation, color, profilePic, socialLinks, userID } = req.body;
  const profile_url = Math.random().toString(36).substring(2, 6);

  try {
    const newUserProfile = new UserProfile({
      name,
      occupation,
      color,
      profilePic,
      socialLinks,
      userID,
      profileCode: profile_url,
    });

    await newUserProfile.save();
    res.status(200).json({ message: "Profile saved successfully" });
  } catch (error) {
    res.status(500).json({ message: "Server error", error: error.message });
  }
});

app.get("/getPages/:userId", async (req, res) => {
  const getPublicProfile = await UserProfile.find({
    userID: req.params.userId,
  });
  res.status(200).json({
    message: "Public profile",
    profiles: getPublicProfile,
  });
});

app.get("/getPage/:pageCode", async (req, res) => {
  const getPublicProfile = await UserProfile.findOne({
    profileCode: req.params.pageCode,
  });
  res.status(200).json({
    message: "Public profile",
    profiles: getPublicProfile,
  });
});

app.get("/remove/:page_id", async (req, res) => {
  const pageId = req.params.page_id;

  const page = await UserProfile.findOne({
    profileCode: pageId,
  });

  if (!page) {
    return next("No product found", 404);
  }

  await page.remove();

  res.status(200).json({
    message: "Page removed",
  });
});

app.listen(PORT, () => console.log(`Server running on port ${PORT}`));
